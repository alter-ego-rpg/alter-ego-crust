use alter_ego_crust::dice::roll_parser::*;
use alter_ego_crust::util::logger::default_logger;
use log::debug;
use std::env;

pub fn main() {
    default_logger();

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: {} <dice_roll>", args[0]);
        return;
    }

    register_default_dice();

    let dice_exp = &args[1..];
    debug!("dice_exp: {:?}", dice_exp);
    let parsed = parse_roll(&dice_exp[0]).unwrap();
    debug!("parsed: {:?}", parsed);
    let results = parsed.roll();
    match results {
        Ok(faces) => {
            println!("{}", faces);
        }
        Err(e) => {
            eprintln!("{e:#?}")
        }
    }
}
