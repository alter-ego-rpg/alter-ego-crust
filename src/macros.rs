#[macro_export]
macro_rules! impl_trait {
    ($tr:ident : $($raw:ty),*) => {
        $(
            impl $tr for $raw {}
        )*
    };
    ($tr:ident : $c_name:ident : $($raw:ty = $val:literal),*) => {
        $(
            impl $tr<$raw> for $raw {
                const $c_name: $raw = $val;
            }
        )*
    };
    ($tr:ident : $c_name:ident = $val:literal : $($raw:ty),*) => {
        $(
            impl $tr<$raw> for $raw {
                const $c_name: $raw = $val;
            }
        )*
    };
}

#[macro_export]
macro_rules! die_box {
    (
        $( [ $($generic:tt)* ] )?
        @ $struct:ty => $die_face:ty
        $( where $($bound:tt)* )?
    ) => {
        impl $(<$($generic)*>)? Die<dyn DieFace + 'static> for Box<$struct>
        $( where $($bound)* )?
        {

            fn new_rng(rng: RngParam) -> Box<Self>
            where
                Self: Sized,
            {
                Box::new(<$struct>::new_rng(rng))
            }

            fn rng(&self) -> DieRng {
                (**self).rng()
            }

            fn get_current_face(&self) -> Box<dyn DieFace + 'static> {
                (**self).get_current_face()
            }

            fn set_current_face(&mut self, face: Box<dyn DieFace + 'static>) {
                // Attempt to downcast `Box<dyn DieFace>` directly to `Box<CoinFace>`.
                let any_face = face.as_any_ref();

                // Now, attempt to downcast `&dyn Any` to `&CoinFace`.
                if let Some(face_ref) = any_face.downcast_ref::<$die_face>() {
                    // Successfully downcasted to `&CoinFace`.
                    // Clone it into `Box<CoinFace>` since `CoinFace` is `Copy`.
                    (**self).set_current_face(Box::new(*face_ref));
                } else {
                    // Handle the error if the downcast was not successful.
                    panic!("Failed to downcast to CoinFace");
                }
            }

            fn num_sides(&self) -> usize {
                (**self).num_sides()
            }

            fn faces(&self) -> Box<dyn Iterator<Item = Box<dyn DieFace + 'static>> + '_> {
                Box::new((**self).faces().map(|face| face as Box<dyn DieFace>))
            }

            fn name(&self) -> Arc<str> {
                (**self).name()
            }

            fn description(&self) -> Arc<str> {
                (**self).description()
            }
        }

        impl $(<$($generic)*>)? Die<$die_face> for Box<$struct>
        $( where $($bound)* )?
        {

            fn new_rng(rng: RngParam) -> Box<Self>
            where
                Self: Sized,
            {
                Box::new(<$struct>::new_rng(rng))
            }

            fn rng(&self) -> DieRng {
                (**self).rng()
            }

            fn get_current_face(&self) -> Box<$die_face> {
                (**self).get_current_face()
            }

            fn set_current_face(&mut self, face: Box<$die_face>) {
                // Attempt to downcast `Box<dyn DieFace>` directly to `Box<CoinFace>`.
                let any_face = face.as_any_ref();

                // Now, attempt to downcast `&dyn Any` to `&CoinFace`.
                if let Some(face_ref) = any_face.downcast_ref::<$die_face>() {
                    // Successfully downcasted to `&CoinFace`.
                    // Clone it into `Box<CoinFace>` since `CoinFace` is `Copy`.
                    (**self).set_current_face(Box::new(*face_ref));
                } else {
                    // Handle the error if the downcast was not successful.
                    panic!("Failed to downcast to CoinFace");
                }
            }

            fn num_sides(&self) -> usize {
                (**self).num_sides()
            }

            fn faces(&self) -> Box<dyn Iterator<Item = Box<$die_face>> + '_> {
                Box::new((**self).faces().map(|face| face))
            }

            fn name(&self) -> Arc<str> {
                (**self).name()
            }

            fn description(&self) -> Arc<str> {
                (**self).description()
            }
        }
    };
}

#[macro_export]
macro_rules! die_face {
    ($struct:ty) => {
        die_face!(@impl $struct: , , );
    };
    ($struct:ty: $feat:ident $($tail:ident)*) => {
        die_face!(@feat($feat $($tail)*) $struct: [,,]);
    };
    (@feat() $struct:ty: [$($compare:ident)?, $($as_debug:ident)?, $($as_display:ident)?]) => {
        die_face!(@impl $struct: $($compare)?, $($as_debug)?, $($as_display)?);
    };
    (@feat(cmp $($tail:ident)*) $struct:ty: [$($compare:ident)?, $($as_debug:ident)?, $($as_display:ident)?]) => {
        die_face!(@feat($($tail)*) $struct: [compare, $($as_debug)?, $($as_display)?]);
    };
    (@feat(debug $($tail:ident)*) $struct:ty: [$($compare:ident)?, $($as_debug:ident)?, $($as_display:ident)?]) => {
        die_face!(@feat($($tail)*) $struct: [$($compare)?, as_debug, $($as_display)?]);
    };
    (@feat(display $($tail:ident)*) $struct:ty: [$($compare:ident)?, $($as_debug:ident)?, $($as_display:ident)?]) => {
        die_face!(@feat($($tail)*) $struct: [$($compare)?, $($as_debug)?, as_display]);
    };
    (@impl $struct:ty: $($compare:ident)?, $($as_debug:ident)?, $($as_display:ident)?) => {

        impl DieFace for $struct {

            fn as_any<'a>(&'a self) -> &'a dyn Any {
                self
            }

            fn equals(&self, other: &dyn DieFace) -> bool {
                let downcast_other = other.as_any().downcast_ref::<$struct>();
                match downcast_other {
                    None => false,
                    Some(true_other) => self == true_other,
                }
            }

            $(
            fn $compare(&self, other: &dyn DieFace) -> Option<std::cmp::Ordering> {
                let downcast_other = other.as_any().downcast_ref::<$struct>();
                match downcast_other {
                    None => None,
                    Some(true_other) => self.partial_cmp(true_other),
                }
            }
            )?

            $(
            fn $as_debug(&self) -> Option<&dyn std::fmt::Debug> {
                Some(self)
            }
            )?

            $(
            fn $as_display(&self) -> Option<&dyn std::fmt::Display> {
                Some(self)
            }
            )?
        }
    };
}

#[macro_export]
macro_rules! dyn_die {
    (
        $( [ $($generic:tt)* ] )?
        @ $struct:ty => $die_face:ty
        $( where $($bound:tt)* )?
    ) => {
        impl $(<$($generic)*>)? Die<dyn DieFace + 'static> for $struct
        $( where $($bound)* )?
        {

            fn new_rng(rng: RngParam) -> Box<Self>
            where
                Self: Sized,
            {
                <$struct>::new_rng(rng)
            }

            fn rng(&self) -> DieRng {
                <$struct>::rng(&(*self))
            }

            fn get_current_face(&self) -> Box<dyn DieFace + 'static> {
                <$struct>::get_current_face(*self)
            }

            fn set_current_face(&mut self, face: Box<dyn DieFace + 'static>) {
                // Attempt to downcast `Box<dyn DieFace>` directly to `Box<CoinFace>`.
                let any_face = face.as_any_ref();

                // Now, attempt to downcast `&dyn Any` to `&CoinFace`.
                if let Some(face_ref) = any_face.downcast_ref::<$die_face>() {
                    // Successfully downcasted to `&CoinFace`.
                    // Clone it into `Box<CoinFace>` since `CoinFace` is `Copy`.
                    (*self).set_current_face(Box::new(*face_ref));
                } else {
                    // Handle the error if the downcast was not successful.
                    panic!("Failed to downcast to {}", stringify!($die_face));
                }
            }

            fn num_sides(&self) -> usize {
                <$struct>::num_sides(&(*self))
            }

            fn faces(&self) -> Box<dyn Iterator<Item = Box<dyn DieFace + 'static>> + '_> {
                Box::new((*self).faces().map(|face| face))
            }

            fn name(&self) -> Arc<str> {
                <$struct>::name(&(*self))
            }

            fn description(&self) -> Arc<str> {
                <$struct>::description(&(*self))
            }
        }
    };
}
