//! All things dice

use std::any::Any;
use std::cmp::Ordering;
use std::fmt::{Debug, Display, Formatter};
use std::sync::{Arc, Mutex};

use dyn_clone::DynClone;
use rand::distr::{Distribution, Uniform};
use rand::prelude::StdRng;
use rand::{RngCore, SeedableRng};
use thiserror::Error;

pub mod astro;
pub mod coin;
pub mod enum_dice;
pub mod pool;
pub mod roll_parser;
pub mod standard;

/// Type alias to simplify signatures for RngCore references
pub type RngParam = Box<dyn RngCore + Send + Sync + 'static>;

/// Error type for DieFace operations.
#[derive(Error, Debug)]
pub enum DieFaceError {
    #[error("casting error")]
    CastingError,
}

/// Encapsulation of Random Number Generators with details specific to dice rolling.
#[derive(Clone)]
pub struct DieRng {
    rng_ref: Arc<Mutex<RngParam>>,
    dist: Uniform<usize>,
}

impl DieRng {
    /// Constructor for DieRng.
    ///
    /// # Arguments
    ///
    /// * `rng` - A random number generator.
    /// * `max` - The upper bound (exclusive) for the uniform distribution.
    ///
    /// # Panics
    ///
    /// Panics if `max` ≤ 0.
    pub fn new(rng: RngParam, max: usize) -> Self {
        DieRng {
            // rng_ref: Arc::new(Mutex::new(Box::new(rng))),
            rng_ref: Arc::new(Mutex::new(rng)),
            dist: Uniform::new(0, max).expect("max should be > 0"),
        }
    }

    /// Gives a random number in the range 0..max (where max is the parameter used in the constructor).
    ///
    /// # Returns
    ///
    /// A random number in the range 0..max.
    pub fn next(&mut self) -> usize {
        // Lock the mutex to get a MutexGuard.
        let mut rng_guard = self.rng_ref.lock().unwrap();
        // Dereference the MutexGuard to get the Box<dyn RngCore + Send + Sync>.
        // Then dereference that to get the RNG itself.
        let face_index = self.dist.sample(&mut *rng_guard);
        face_index
    }
}

/// Trait representing a face of a die.
pub trait DieFace: DynClone + Any {
    /// Returns a reference to the face as a `dyn Any`.
    fn as_any<'a>(&'a self) -> &'a dyn Any;
    fn equals(&self, other: &dyn DieFace) -> bool;

    fn compare(&self, _other: &dyn DieFace) -> Option<Ordering> {
        None
    }

    // Provide methods to optionally get Debug and Display references
    fn as_debug(&self) -> Option<&dyn Debug> {
        None
    }
    fn as_display(&self) -> Option<&dyn Display> {
        None
    }
}

impl<T: 'static> AsRef<T> for dyn DieFace {
    fn as_ref(&self) -> &T {
        self.as_any().downcast_ref().unwrap()
    }
}

pub trait SortableDieFace: DieFace + Ord {}

impl From<Box<dyn DieFace>> for Box<dyn Any> {
    fn from(value: Box<dyn DieFace>) -> Self {
        value
    }
}

impl Debug for dyn DieFace {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(debug) = self.as_debug() {
            write!(f, "{:?}", debug)
        } else if let Some(display) = self.as_display() {
            write!(f, "{}", display)
        } else {
            write!(f, "dyn DieFace")
        }
    }
}

// impl PartialEq for dyn DieFace {
//     fn eq(&self, other: &Self) -> bool {
//         if self.type_id() != other.type_id() {
//             return false;
//         }
//
//         // Attempt to downcast both self and other to their concrete types
//         let self_any = self.as_any();
//         let self_cast = self_any.downcast_ref::<&Self>();
//         let other_any = other.as_any();
//         let other_cast = other_any.downcast_ref::<&Self>();
//
//         // Compare the downcasted values
//         self_cast == other_cast
//     }
// }

/// Wrapper class for DieFace trait objects.
#[derive(Debug)]
pub struct DynDieFace {
    die_face: Box<dyn DieFace>,
}

impl PartialEq for DynDieFace {
    fn eq(&self, other: &Self) -> bool {
        self.die_face.equals(&*other.die_face)
    }
}

impl PartialOrd for DynDieFace {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        return self.die_face.compare(&*other.die_face);
    }
}

impl Display for DynDieFace {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self.die_face.as_display() {
            Some(ddf) => {
                write!(f, "{}", ddf)
            }
            None => match self.die_face.as_debug() {
                Some(ddf) => {
                    write!(f, "{:#?}", ddf)
                }
                None => {
                    unimplemented!("Missing die face display or debug implementation")
                }
            },
        }
    }
}

/// Wrapper class for DieFace trait objects.
impl DynDieFace {
    /// Creates a new DieFaceData.
    ///
    /// # Arguments
    ///
    /// * `face_value` - A boxed DieFace trait object.
    ///
    /// # Returns
    ///
    /// A new instance of DieFaceData.
    pub fn new(face_value: Box<dyn DieFace>) -> Self {
        Self {
            die_face: face_value,
        }
    }

    /// Attempts to downcast the DieFaceData to a specific type.
    ///
    /// # Type Parameters
    ///
    /// * `T` - The target type to downcast to.
    ///
    /// # Returns
    ///
    /// A Result containing the downcasted value or a DieFaceError.
    pub fn value<T>(self) -> Result<Box<T>, DieFaceError>
    where
        T: DieFace,
    {
        let self_clone = dyn_clone::clone_box(&*self.die_face);
        // let result: Box<dyn Any> = Box::new(self.value.as_any());
        let result: Box<dyn Any> = self_clone.into();
        result
            .downcast()
            .or(Err(DieFaceError::CastingError))
    }

    /// Returns a cloned boxed DieFace trait object.
    ///
    /// # Returns
    ///
    /// A cloned boxed DieFace trait object.
    pub fn dyn_value(&self) -> Box<dyn DieFace> {
        dyn_clone::clone_box(&*self.die_face)
    }
}

/// Trait for the creation of instances of Die.
pub trait NewDie
where
    Self: 'static,
{
    /// Creates a new instance of the die with a seeded RNG.
    ///
    /// # Arguments
    ///
    /// * `seed` - The seed for the RNG.
    ///
    /// # Returns
    ///
    /// A new instance of the die.
    #[cfg(test)]
    fn new_seeded(seed: u64) -> Self
    where
        Self: Sized,
    {
        Self::new_rng(Box::new(StdRng::seed_from_u64(seed)))
        // Self::new_rng(StdRng::seed_from_u64(seed))
    }

    /// Constructor which requires a `RngParam`.
    ///
    /// # Arguments
    ///
    /// * `rng` - A random number generator.
    ///
    /// # Returns
    ///
    /// A new instance of the die.
    fn new_rng(rng: RngParam) -> Self
    where
        Self: Sized;

    /// Default constructor.
    ///
    /// # Returns
    ///
    /// A new instance of the die.
    fn new() -> Self
    where
        Self: Sized,
    {
        Self::new_rng(Box::new(StdRng::from_os_rng()))
        // Self::new_rng(StdRng::from_entropy())
    }
}

/// Trait representing a die.
pub trait Die {
    /// Get the `DieRng` for the implementation.
    ///
    /// # Returns
    ///
    /// The `DieRng` instance.
    fn rng(&self) -> DieRng;

    /// Roll the die, and stores the face of the result.
    ///
    /// To get the result, make a separate call to the `current_face` method.
    fn roll(&mut self) {
        let face_index = self.rng().next();
        let face = self
            .faces()
            .nth(face_index)
            .expect("Invalid face index");
        self.set_current_face(face);
    }

    /// Get the current face of the die.
    ///
    /// # Returns
    ///
    /// The current face of the die.
    fn get_current_face(&self) -> DynDieFace;

    /// Set the current face of the die.
    ///
    /// # Arguments
    ///
    /// * `face` - The face to set as the current face.
    fn set_current_face(&mut self, face: DynDieFace);

    /// Get the number of sides of the die.
    ///
    /// # Returns
    ///
    /// The number of sides of the die.
    fn num_sides(&self) -> usize;

    /// Get an iterator over the faces of the die instance.
    ///
    /// In most cases this should be the same as model_faces(), but the distinction allows for
    /// some implementations to have different faces for each instance or event to have mutable
    /// faces for their instances.
    ///
    /// # Returns
    ///
    /// An iterator over the faces of the die.
    fn faces(&self) -> Box<dyn Iterator<Item = DynDieFace> + '_>;

    /// Get an iterator over the faces of the die model (class).
    ///
    /// The optional-ness of the result allows for some implementations to not determine their
    /// faces at the class level, and only at the instance level.
    ///
    /// # Returns
    ///
    /// An iterator over the faces of the die model, or None if not defined for the class.
    fn model_faces() -> Option<Box<dyn Iterator<Item = DynDieFace> + 'static>>
    where
        Self: Sized;

    /// Get the name of the die.
    ///
    /// # Returns
    ///
    /// The name of the die.
    fn name(&self) -> Arc<str>;

    /// Get the description of the die.
    ///
    /// # Returns
    ///
    /// The description of the die.
    fn description(&self) -> Arc<str>;
}

/// Wrapper type for using dice in dynamic contexts, such as DicePool.
pub struct DynDie {
    die: Box<dyn Die>,
}

impl DynDie {
    /// Creates a new DynDie from an existing die instance.
    ///
    /// # Arguments
    ///
    /// * `die` - A boxed die instance.
    ///
    /// # Returns
    ///
    /// A new instance of DynDie.
    pub fn from_instance(die: Box<dyn Die>) -> Self {
        Self { die }
    }

    /// Creates a new DynDie with a seeded RNG.
    ///
    /// # Arguments
    ///
    /// * `seed` - The seed for the RNG.
    ///
    /// # Type Parameters
    ///
    /// * `T` - The type of the die.
    ///
    /// # Returns
    ///
    /// A new instance of DynDie.
    #[cfg(test)]
    pub fn new_seeded<T>(seed: u64) -> Self
    where
        T: Die + NewDie + Sized,
    {
        Self {
            die: Box::new(T::new_seeded(seed)),
        }
    }

    /// Constructor which requires a `RngParam`.
    ///
    /// # Arguments
    ///
    /// * `rng` - A random number generator.
    ///
    /// # Type Parameters
    ///
    /// * `T` - The type of the die.
    ///
    /// # Returns
    ///
    /// A new instance of DynDie.
    pub fn new_rng<T>(rng: RngParam) -> Self
    where
        T: Die + NewDie + Sized,
    {
        Self {
            die: Box::new(T::new_rng(rng)),
        }
    }

    /// Default constructor.
    ///
    /// # Type Parameters
    ///
    /// * `T` - The type of the die.
    ///
    /// # Returns
    ///
    /// A new instance of DynDie.
    pub fn new<T>() -> Self
    where
        T: Die + NewDie + Sized,
    {
        Self {
            die: Box::new(T::new()),
        }
    }
}

impl Die for DynDie {
    fn rng(&self) -> DieRng {
        self.die.rng()
    }

    fn get_current_face(&self) -> DynDieFace {
        self.die.get_current_face()
    }

    fn set_current_face(&mut self, face: DynDieFace) {
        self.die.set_current_face(face)
    }

    fn num_sides(&self) -> usize {
        self.die.num_sides()
    }

    fn faces(&self) -> Box<dyn Iterator<Item = DynDieFace> + '_> {
        Box::new(self.die.faces().map(|face| face))
    }

    fn model_faces() -> Option<Box<dyn Iterator<Item = DynDieFace> + 'static>> {
        None
    }

    fn name(&self) -> Arc<str> {
        self.die.name().clone()
    }

    fn description(&self) -> Arc<str> {
        self.die.description().clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dice::DynDie;
    use core_extensions::__::usize;
    use rand::rngs::mock::StepRng;
    use rstest::rstest;
    use rstest_reuse::*;
    use std::any::TypeId;

    use crate::dice::{
        astro::{DPlanet, DZodiac, Planet, Zodiac},
        coin::{Coin, CoinFace},
        standard::{D4, D6, D8, D10, D12, D20, NumFace},
    };
    use crate::die_face;

    fn step(sides: u64) -> u64 {
        u64::MAX / sides
    }

    #[template]
    #[rstest]
    #[case::d4(DynDie::new::<D4>, 4, TypeId::of::<NumFace>())]
    #[case::d6(DynDie::new::<D6>, 6, TypeId::of::<NumFace>())]
    #[case::d8(DynDie::new::<D8>, 8, TypeId::of::<NumFace>())]
    #[case::d10(DynDie::new::<D10>, 10, TypeId::of::<NumFace>())]
    #[case::d12(DynDie::new::<D12>, 12, TypeId::of::<NumFace>())]
    #[case::d20(DynDie::new::<D20>, 20, TypeId::of::<NumFace>())]
    #[case::coin(DynDie::new::<Coin>, 2, TypeId::of::<CoinFace>())]
    #[case::zodiac(DynDie::new::<DZodiac>, 12, TypeId::of::<Zodiac>())]
    #[case::planet(DynDie::new::<DPlanet>, 10, TypeId::of::<Planet>())]
    fn dyn_die_cases(
        #[case] new_die: fn() -> DynDie,
        #[case] sides: usize,
        #[case] face_type_id: TypeId,
    ) {
    }

    #[apply(dyn_die_cases)]
    fn test_dyn_die_size(
        #[case] new_die: fn() -> DynDie,
        #[case] sides: usize,
        #[case] _face_type_id: TypeId,
    ) {
        let die = new_die();
        assert_eq!(die.num_sides(), sides);
    }

    #[apply(dyn_die_cases)]
    fn test_dyn_die_roll(
        #[case] new_die: fn() -> DynDie,
        #[case] _sides: usize,
        #[case] face_type_id: TypeId,
    ) {
        let mut die = new_die();
        die.roll();
        let result = die.get_current_face();
        let inner_type_id = (*result.dyn_value()).type_id();
        assert_eq!(inner_type_id, face_type_id);
    }

    #[apply(dyn_die_cases)]
    fn test_die_rng_initialization(
        #[case] _new_die: fn() -> DynDie,
        #[case] sides: usize,
        #[case] _face_type_id: TypeId,
    ) {
        let s = step(sides as u64);
        let rng = Box::new(StepRng::new(s * 2, s));
        let die_rng = DieRng::new(rng, sides);
        assert_eq!(
            die_rng
                .dist
                .sample(&mut *die_rng.rng_ref.lock().unwrap()),
            1
        );
    }

    #[apply(dyn_die_cases)]
    fn test_die_rng_next(
        #[case] _new_die: fn() -> DynDie,
        #[case] sides: usize,
        #[case] _face_type_id: TypeId,
    ) {
        const ROLLS: usize = 40;
        let s = step(sides as u64);
        let rng = Box::new(StepRng::new(s, s));
        let mut die_rng = DieRng::new(rng, sides);
        let mut result = Vec::with_capacity(ROLLS);
        let mut expected = Vec::with_capacity(ROLLS);
        for i in 0..ROLLS {
            result.push(die_rng.next());
            expected.push(i % sides);
        }

        assert_eq!(result, expected);
    }

    #[test]
    fn test_die_face_data_casting() {
        #[derive(Clone, Debug, PartialEq)]
        struct TestFace;
        die_face!(TestFace);

        let face = Box::new(TestFace);
        let die_face_data = DynDieFace::new(face);

        let result: Result<Box<TestFace>, DieFaceError> = die_face_data.value();
        assert!(result.is_ok());
    }

    #[test]
    fn test_die_face_data_casting_error() {
        #[derive(Clone, Debug, PartialEq)]
        struct TestFace;
        die_face!(TestFace);

        #[derive(Clone, Debug, PartialEq)]
        struct AnotherFace;
        die_face!(AnotherFace);

        let face = Box::new(TestFace);
        let die_face_data = DynDieFace::new(face);

        let result: Result<Box<AnotherFace>, DieFaceError> = die_face_data.value();
        assert!(result.is_err());
    }

    #[test]
    fn test_dyn_die_new_seeded() {
        let die = DynDie::new_seeded::<D6>(42);
        assert_eq!(die.num_sides(), 6);
    }

    #[test]
    fn test_dyn_die_new_rng() {
        let rng = Box::new(StepRng::new(2, 1));
        let die = DynDie::new_rng::<D6>(rng);
        assert_eq!(die.num_sides(), 6);
    }
}
