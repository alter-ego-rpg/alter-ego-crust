use backtrace::{Backtrace, BacktraceFrame, Symbol, SymbolName};
use colog::format::{CologStyle, DefaultCologStyle, default_prefix_token};
use colored::Colorize;
use env_logger::Builder;
use lazy_static::lazy_static;
use log::{Level, debug};
use std::panic::Location;

const RELEVANT_CALLER_FRAME: usize = 15;
const UNKNOWN: &str = "unknown";
// lazy_static! {
//     static ref UNKNOWN_SYMBOL: SymbolName<'static> = SymbolName::new("unknown".as_bytes());
// }

struct AlterEgoLoggerStyle;

impl CologStyle for AlterEgoLoggerStyle {
    // #[track_caller]
    fn prefix_token(&self, level: &Level) -> String {
        if level < &Level::Debug {
            return default_prefix_token(&DefaultCologStyle, level);
        }
        let btrace = Backtrace::new_unresolved();
        let frames = btrace.frames();
        if frames.len() <= RELEVANT_CALLER_FRAME {
            return default_prefix_token(&DefaultCologStyle, level);
        }
        let mut frame = frames[RELEVANT_CALLER_FRAME].clone();
        frame.resolve();
        let sym = frame.symbols().first().unwrap();
        let filename = match sym.filename() {
            None => UNKNOWN,
            Some(f) => f.file_name().unwrap().to_str().unwrap(),
        };
        format!(
            "[{}] [{:}:{:}] [{:}]\n",
            self.level_color(level, self.level_token(level)),
            filename.bold(),
            sym.lineno().unwrap_or(0),
            sym.name()
                .unwrap_or(SymbolName::new(UNKNOWN.as_bytes()))
                .as_str()
                .unwrap()
                .yellow()
        )
    }
}

pub fn default_logger() {
    let mut clog = Builder::new();
    clog.format(colog::formatter(AlterEgoLoggerStyle));
    clog.filter(None, log::LevelFilter::Debug);

    clog.init();
}
