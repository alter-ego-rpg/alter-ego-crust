//! All things character

use std::collections::HashMap;
use std::path::Path;
use std::sync::Arc;

// pub mod player_character;
// pub mod non_player_character;

pub trait Character {
    fn name(&self) -> String;
    fn description(&self) -> String;
    fn items(&self) -> HashMap<String, usize>;
    fn stats(&self) -> HashMap<String, String>;
}

pub trait Player<C: Character> {
    fn name(&self) -> String;
    fn character(&self) -> Arc<C>;
}
