use crate::dice::DieFace;
use crate::dice::SortableDieFace;
use crate::dice::enum_dice::{DiceEnum, EnumDie, EnumDieName};
use crate::die_face;
// use dyn_clone::DynClone;
use std::any::Any;
use std::fmt::{Debug, Display};

use strum_macros::{EnumCount, IntoStaticStr, VariantArray};

#[derive(
    Copy, Clone, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, VariantArray, EnumCount, IntoStaticStr,
)]
pub enum Zodiac {
    ARIES,
    TAURUS,
    GEMINI,
    CANCER,
    LEO,
    VIRGO,
    LIBRA,
    SCORPIO,
    SAGITTARIUS,
    CAPRICORN,
    AQUARIUS,
    PISCES,
}

impl EnumDieName for Zodiac {
    fn die_name() -> String {
        "Zodiac".to_string()
    }

    fn face_name(&self) -> String {
        let name: &str = self.into();
        name.to_string()
    }

    fn face_char(&self) -> char {
        use Zodiac::*;
        match self {
            ARIES => '\u{2648}',
            TAURUS => '\u{2649}',
            GEMINI => '\u{264A}',
            CANCER => '\u{264B}',
            LEO => '\u{264C}',
            VIRGO => '\u{264D}',
            LIBRA => '\u{264E}',
            SCORPIO => '\u{264F}',
            SAGITTARIUS => '\u{2650}',
            CAPRICORN => '\u{2651}',
            AQUARIUS => '\u{2652}',
            PISCES => '\u{2653}',
        }
    }
}

impl Display for Zodiac {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.face_char()))
    }
}

impl DiceEnum for Zodiac {}

impl SortableDieFace for Zodiac {}

die_face!(Zodiac: debug cmp);

#[derive(Copy, Clone, Debug, Hash, PartialEq, Eq, VariantArray, EnumCount, IntoStaticStr)]
pub enum Planet {
    SUN,
    MOON,
    MERCURY,
    VENUS,
    MARS,
    JUPITER,
    SATURN,
    URANUS,
    NEPTUNE,
    PLUTO,
}

impl EnumDieName for Planet {
    fn die_name() -> String {
        "Planet".to_string()
    }

    fn face_name(&self) -> String {
        let name: &str = self.into();
        name.to_string()
    }

    fn face_char(&self) -> char {
        use Planet::*;
        match self {
            SUN => '\u{2609}',
            MOON => '\u{263D}',
            MERCURY => '\u{263F}',
            VENUS => '\u{2640}',
            MARS => '\u{2642}',
            JUPITER => '\u{2643}',
            SATURN => '\u{2644}',
            URANUS => '\u{2645}',
            NEPTUNE => '\u{2646}',
            PLUTO => '\u{2647}',
        }
    }
}

impl Display for Planet {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_fmt(format_args!("{}", self.face_char()))
    }
}

impl DiceEnum for Planet {}

die_face!(Planet: debug);

pub type DZodiac = EnumDie<Zodiac>;
pub type DPlanet = EnumDie<Planet>;

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dice::enum_dice::{DiceEnum, EnumDie, EnumDieName};
    use crate::dice::{Die, DieFace, NewDie};
    use core::hash::Hash;
    use rstest::rstest;
    use rstest_reuse::*;
    use std::collections::HashMap;
    use std::fmt::Debug;
    use strum::EnumCount;

    #[template]
    #[rstest]
    #[case::zodiac(DZodiac::new, Zodiac::COUNT)]
    #[case::planet(DPlanet::new, Planet::COUNT)]
    fn enum_die_cases<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] sides: usize)
    where
        T: DiceEnum + Copy + DieFace,
    {
    }

    #[apply(enum_die_cases)]
    fn test_enum_die_num_sides<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] expected: usize)
    where
        T: DiceEnum + Copy + DieFace,
    {
        let die = new_die();
        assert_eq!(expected, die.num_sides());
    }

    #[apply(enum_die_cases)]
    fn test_enum_die_roll<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] _max: usize)
    where
        T: DiceEnum + Copy + DieFace + PartialEq,
    {
        let mut die = new_die();
        for _ in 0..20 {
            die.roll();
            let result = die
                .get_current_face()
                .value::<T>()
                .expect("Should be a DiceEnum type");
            assert!(T::VARIANTS.contains(&result));
        }
    }

    // Helper function to roll a die multiple times and count the occurrences of each outcome
    fn roll_and_count_enum<T>(die: &mut EnumDie<T>, num_rolls: usize) -> HashMap<T, usize>
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash,
    {
        let mut counts: HashMap<T, usize> = HashMap::new();
        for _ in 0..num_rolls {
            die.roll();
            let outcome: Box<T> = die
                .get_current_face()
                .value()
                .expect("Should be a DiceEnum type");
            *counts.entry(*outcome).or_insert(0) += 1;
        }
        counts
    }

    // Test to check if the distribution of outcomes is uniform
    #[apply(enum_die_cases)]
    fn test_enum_distribution_bias<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] sides: usize)
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash + Debug,
    {
        let mut die = new_die();
        let num_rolls = 100000; // Number of rolls to simulate
        let counts = roll_and_count_enum(&mut die, num_rolls);

        // Calculate the expected frequency for each outcome
        let expected_frequency = num_rolls as f64 / sides as f64;

        // Check if the actual frequency is within a reasonable range of the expected frequency
        for (outcome, count) in counts {
            let actual_frequency = count as f64;
            let delta = (actual_frequency - expected_frequency).abs() / num_rolls as f64;
            assert!(
                delta <= 0.01,
                "Outcome {:?} has a bias. Expected frequency: {:?}, Actual frequency: {:?}, Delta: {:?}",
                outcome,
                expected_frequency,
                actual_frequency,
                delta
            );
        }
    }

    #[rstest]
    #[case::zodiac(DZodiac::new, "Zodiac (D12)")]
    #[case::planet(DPlanet::new, "Planet (D10)")]
    fn test_enum_die_name<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] expected: &str)
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash + Debug,
    {
        let die = new_die();
        assert_eq!(die.name().as_ref(), expected);
    }

    #[rstest]
    #[case::zodiac(DZodiac::new, "A die with 12 sides representing Zodiac.")]
    #[case::planet(DPlanet::new, "A die with 10 sides representing Planet.")]
    fn test_enum_die_description<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] expected: &str)
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash + Debug,
    {
        let die = new_die();
        assert_eq!(die.description().as_ref(), expected);
    }

    #[test]
    fn test_enum_die_face_name() {
        let die_zodiac = DZodiac::new();
        let face_zodiac = die_zodiac
            .get_current_face()
            .value::<Zodiac>()
            .unwrap();
        assert!(matches!(
            *face_zodiac,
            Zodiac::ARIES
                | Zodiac::TAURUS
                | Zodiac::GEMINI
                | Zodiac::CANCER
                | Zodiac::LEO
                | Zodiac::VIRGO
                | Zodiac::LIBRA
                | Zodiac::SCORPIO
                | Zodiac::SAGITTARIUS
                | Zodiac::CAPRICORN
                | Zodiac::AQUARIUS
                | Zodiac::PISCES
        ));

        let die_planet = DPlanet::new();
        let face_planet = die_planet
            .get_current_face()
            .value::<Planet>()
            .unwrap();
        assert!(matches!(
            *face_planet,
            Planet::SUN
                | Planet::MOON
                | Planet::MERCURY
                | Planet::VENUS
                | Planet::MARS
                | Planet::JUPITER
                | Planet::SATURN
                | Planet::URANUS
                | Planet::NEPTUNE
                | Planet::PLUTO
        ));
    }

    #[test]
    fn test_enum_die_face_char() {
        let die_zodiac = DZodiac::new();
        let face_zodiac = die_zodiac
            .get_current_face()
            .value::<Zodiac>()
            .unwrap();
        assert!(matches!(
            face_zodiac.face_char(),
            '\u{2648}'
                | '\u{2649}'
                | '\u{264A}'
                | '\u{264B}'
                | '\u{264C}'
                | '\u{264D}'
                | '\u{264E}'
                | '\u{264F}'
                | '\u{2650}'
                | '\u{2651}'
                | '\u{2652}'
                | '\u{2653}'
        ));

        let die_planet = DPlanet::new();
        let face_planet = die_planet
            .get_current_face()
            .value::<Planet>()
            .unwrap();
        assert!(matches!(
            face_planet.face_char(),
            '\u{2609}'
                | '\u{263D}'
                | '\u{263F}'
                | '\u{2640}'
                | '\u{2642}'
                | '\u{2643}'
                | '\u{2644}'
                | '\u{2645}'
                | '\u{2646}'
                | '\u{2647}'
        ));
    }
}
