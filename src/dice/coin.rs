use crate::dice::{Die, DieFace, DieRng, DynDieFace, NewDie, RngParam};
use std::any::Any;
use std::fmt::{Debug, Display};
use std::hash::{Hash, Hasher};
use std::sync::Arc;

use crate::die_face;

use arcstr::{ArcStr, literal};
use uuid::Uuid;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum CoinFace {
    HEADS,
    TAILS,
}

pub const COIN_FACES: [CoinFace; 2] = [CoinFace::HEADS, CoinFace::TAILS];
const COIN_NAME: ArcStr = literal!("Coin");
const COIN_DESCRIPTION: ArcStr =
    literal!("A standard coin with distinct sides, one for Heads, the other for Tails.");

pub struct Coin {
    id: Uuid,
    rng: DieRng,
    current_face: CoinFace,
}

impl PartialEq for Coin {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Coin {}

impl Hash for Coin {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

die_face!(CoinFace: debug);

impl NewDie for Coin {
    fn new_rng(rng: RngParam) -> Self
    where
        Self: Sized,
    {
        Self {
            id: Uuid::new_v4(),
            rng: DieRng::new(rng, 2),
            current_face: CoinFace::HEADS,
        }
    }
}

impl Die for Coin {
    fn rng(&self) -> DieRng {
        self.rng.clone()
    }

    fn get_current_face(&self) -> DynDieFace {
        DynDieFace::new(Box::new(self.current_face))
    }

    fn set_current_face(&mut self, face: DynDieFace) {
        self.current_face = *face
            .value()
            .expect("Should be a CoinFace");
    }

    fn num_sides(&self) -> usize {
        2
    }

    fn faces(&self) -> Box<dyn Iterator<Item = DynDieFace> + '_> {
        Self::model_faces().unwrap()
    }

    fn model_faces() -> Option<Box<dyn Iterator<Item = DynDieFace> + 'static>> {
        Some(Box::new(
            COIN_FACES
                .iter()
                .map(|face| DynDieFace::new(dyn_clone::clone_box(face))),
        ))
    }

    fn name(&self) -> Arc<str> {
        COIN_NAME.into()
    }

    fn description(&self) -> Arc<str> {
        COIN_DESCRIPTION.into()
    }
}
