use crate::dice::astro::Planet;
use crate::dice::{Die, DieFace, DieRng, DynDie, DynDieFace, NewDie, RngParam};
use crate::{
    // die_box,
    dyn_die,
};
use lazy_static::lazy_static;
use std::collections::HashMap;
use std::fmt::{Debug, Display};
use std::hash::{Hash, Hasher};
use std::sync::{Arc, Mutex};
use strum::{EnumCount, VariantArray};
use uuid::Uuid;

/// Marker trait for enums we want to use for dice.
pub trait DiceEnum: EnumCount + VariantArray + EnumDieName {}

lazy_static! {
    static ref META_REGISTRY: Mutex<HashMap<String, Arc<MetaEnumDie>>> = Mutex::new(HashMap::new());
}

pub struct MetaEnumDie {
    name: Arc<str>,
    description: Arc<str>,
}

impl MetaEnumDie {
    pub fn new<T>() -> Self
    where
        T: DiceEnum + Copy + DieFace,
    {
        Self {
            name: format!(
                "{die_name} (D{sides})",
                sides = T::COUNT,
                die_name = T::die_name()
            )
            .into(),
            description: format!(
                "A die with {sides} sides representing {die_name}.",
                sides = T::COUNT,
                die_name = T::die_name()
            )
            .into(),
        }
    }
}

pub struct EnumDie<T>
where
    T: DiceEnum,
{
    id: Uuid,
    meta: Arc<MetaEnumDie>,
    rng: DieRng,
    face: T,
}

impl<T> PartialEq for EnumDie<T>
where
    T: DiceEnum + Copy + DieFace,
{
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<T> Eq for EnumDie<T> where T: DiceEnum + Copy + DieFace {}

impl<T> Hash for EnumDie<T>
where
    T: DiceEnum + Copy + DieFace,
{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl<T> NewDie for EnumDie<T>
where
    T: DiceEnum + Copy + DieFace,
{
    fn new_rng(rng: RngParam) -> Self
    where
        Self: Sized,
    {
        let mut meta_registry = META_REGISTRY.lock().unwrap();
        let meta = meta_registry
            .entry(T::die_name())
            .or_insert_with(|| {
                let meta = MetaEnumDie::new::<T>();
                Arc::new(meta)
            })
            .clone();
        let mut rng = DieRng::new(rng, T::COUNT);
        let current_face = { T::VARIANTS[rng.next()] };

        Self {
            id: Uuid::new_v4(),
            meta,
            rng,
            face: current_face,
        }
    }
}

impl<T> Die for EnumDie<T>
where
    T: DiceEnum + Copy + DieFace,
{
    fn rng(&self) -> DieRng {
        self.rng.clone()
    }

    fn get_current_face(&self) -> DynDieFace {
        DynDieFace {
            die_face: Box::new(self.face),
        }
    }

    fn set_current_face(&mut self, face: DynDieFace) {
        self.face = *face
            .value()
            .expect(format!("Should be DiceEnum type {}", stringify!(T)).as_str());
    }

    fn num_sides(&self) -> usize {
        T::COUNT
    }

    fn faces(&self) -> Box<dyn Iterator<Item=DynDieFace> + '_> {
        Self::model_faces().unwrap()
    }

    fn model_faces() -> Option<Box<dyn Iterator<Item=DynDieFace> + 'static>> {
        Some(Box::new(
            T::VARIANTS
                .iter()
                .map(|face| DynDieFace::new(dyn_clone::clone_box(face))),
        ))
    }

    fn name(&self) -> Arc<str> {
        self.meta.name.clone()
    }

    fn description(&self) -> Arc<str> {
        self.meta.description.clone()
    }
}

pub trait EnumDieName {
    fn die_name() -> String;
    fn face_name(&self) -> String;
    fn face_char(&self) -> char;
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::dice::{Die, DieFace, NewDie};
    use crate::die_face;
    use rstest::{fixture, rstest};
    use rstest_reuse::*;
    use std::any::Any;
    use std::collections::HashMap;
    use std::fmt::Debug;
    use strum_macros::{EnumCount, IntoStaticStr, VariantArray};

    #[derive(Copy, Clone, Debug, PartialEq, VariantArray, EnumCount, IntoStaticStr, Eq, Hash)]
    enum TestEnum {
        Variant1,
        Variant2,
        Variant3,
    }

    die_face!(TestEnum: debug);

    impl EnumDieName for TestEnum {
        fn die_name() -> String {
            "TestEnum".to_string()
        }

        fn face_name(&self) -> String {
            format!("{:?}", self)
        }

        fn face_char(&self) -> char {
            match self {
                TestEnum::Variant1 => 'A',
                TestEnum::Variant2 => 'B',
                TestEnum::Variant3 => 'C',
            }
        }
    }

    impl DiceEnum for TestEnum {}

    #[fixture]
    fn test_enum_die() -> EnumDie<TestEnum> {
        EnumDie::<TestEnum>::new()
    }

    #[fixture]
    fn test_enum_sides() -> usize {
        3
    }

    #[template]
    #[rstest]
    #[case::test_enum(EnumDie::<TestEnum>::new, TestEnum::COUNT)]
    fn enum_die_cases<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] sides: usize)
    where
        T: DiceEnum + Copy + DieFace,
    {}

    #[apply(enum_die_cases)]
    fn test_enum_die_num_sides<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] expected: usize)
    where
        T: DiceEnum + Copy + DieFace,
    {
        let die = new_die();
        assert_eq!(expected, die.num_sides());
    }

    #[rstest]
    fn test_enum_die_roll(mut test_enum_die: EnumDie<TestEnum>) {
        for _ in 0..20 {
            test_enum_die.roll();
            let result = test_enum_die
                .get_current_face()
                .value::<TestEnum>()
                .expect("Should be a DiceEnum type");
            assert!(TestEnum::VARIANTS.contains(&result));
        }
    }

    // Helper function to roll a die multiple times and count the occurrences of each outcome
    fn roll_and_count_enum<T>(die: &mut EnumDie<T>, num_rolls: usize) -> HashMap<T, usize>
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash,
    {
        let mut counts: HashMap<T, usize> = HashMap::new();
        for _ in 0..num_rolls {
            die.roll();
            let outcome: Box<T> = die
                .get_current_face()
                .value()
                .expect("Should be a DiceEnum type");
            *counts.entry(*outcome).or_insert(0) += 1;
        }
        counts
    }

    // Test to check if the distribution of outcomes is uniform
    #[apply(enum_die_cases)]
    fn test_enum_distribution_bias<T>(#[case] new_die: fn() -> EnumDie<T>, #[case] sides: usize)
    where
        T: DiceEnum + Copy + DieFace + Eq + Hash + Debug,
    {
        let mut die = new_die();
        let num_rolls = 100000; // Number of rolls to simulate
        let counts = roll_and_count_enum(&mut die, num_rolls);

        // Calculate the expected frequency for each outcome
        let expected_frequency = num_rolls as f64 / sides as f64;

        // Check if the actual frequency is within a reasonable range of the expected frequency
        for (outcome, count) in counts {
            let actual_frequency = count as f64;
            let delta = (actual_frequency - expected_frequency).abs() / num_rolls as f64;
            assert!(
                delta <= 0.01,
                "Outcome {:?} has a bias. Expected frequency: {:?}, Actual frequency: {:?}, Delta: {:?}",
                outcome,
                expected_frequency,
                actual_frequency,
                delta
            );
        }
    }

    #[rstest]
    fn test_meta_enum_die_initialization() {
        let meta = MetaEnumDie::new::<TestEnum>();
        assert_eq!(meta.name.as_ref(), "TestEnum (D3)");
        assert_eq!(
            meta.description.as_ref(),
            "A die with 3 sides representing TestEnum."
        );
    }

    #[rstest]
    fn test_enum_die_name(test_enum_die: EnumDie<TestEnum>) {
        assert_eq!(test_enum_die.name().as_ref(), "TestEnum (D3)");
    }

    #[rstest]
    fn test_enum_die_description(test_enum_die: EnumDie<TestEnum>) {
        assert_eq!(
            test_enum_die.description().as_ref(),
            "A die with 3 sides representing TestEnum."
        );
    }

    #[rstest]
    fn test_enum_die_face_name(test_enum_die: EnumDie<TestEnum>) {
        let face = test_enum_die
            .get_current_face()
            .value::<TestEnum>()
            .unwrap();
        assert!(matches!(
            &*face.face_name(),
            "Variant1" | "Variant2" | "Variant3"
        ));
    }

    #[rstest]
    fn test_enum_die_face_char(test_enum_die: EnumDie<TestEnum>) {
        let face = test_enum_die
            .get_current_face()
            .value::<TestEnum>()
            .unwrap();
        assert!(matches!(face.face_char(), 'A' | 'B' | 'C'));
    }
}
