use crate::dice::pool::{DicePool, RollResults};
use crate::dice::{DynDie, DynDieFace};
use lazy_static::lazy_static;
use log::debug;
use pest::iterators::{Pair};
use pest::{Parser};
use pest_derive::Parser;
use std::collections::HashMap;
use std::sync::Mutex;
use strum_macros::Display;
use thiserror::Error;

lazy_static! {
    static ref KNOWN_DICE: Mutex<HashMap<String, fn() -> DynDie>> = Mutex::new(HashMap::new());
}

pub fn register_die_constructor(name: String, new_die: fn() -> DynDie) {
    KNOWN_DICE
        .lock()
        .unwrap()
        .insert(name.to_lowercase(), new_die);
}

#[macro_export]
macro_rules! register_die_type {
    ($($die_type:ty)*) => {{
        use $crate::dice::roll_parser::register_die_constructor;
        use $crate::dice::DynDie;
        $(
            register_die_constructor(stringify!($die_type).to_string(), DynDie::new::<$die_type>);
        )*
    }};
}

pub fn register_default_dice() {
    use crate::dice::astro::{DPlanet, DZodiac};
    use crate::dice::coin::Coin;
    use crate::dice::standard::{D4, D6, D8, D10, D12, D20};

    register_die_type!(D4 D6 D8 D10 D12 D20 DPlanet DZodiac Coin);
}

#[derive(Parser)]
#[grammar = "dice/dice.pest"]
struct DiceParser;

#[derive(Error, Debug, Display, Eq, PartialEq)]
pub enum DiceParseError {
    UnrecognizedFormat(String),
    DiceTypeNotFound(String),
    NoTypeFound,
}

#[derive(Default, Debug)]
pub struct ParsedDiceExpression {
    die_count: Option<usize>,
    die_type: Option<String>,
    mod_type: Option<String>,
    mod_count: Option<usize>,
}

impl ParsedDiceExpression {
    pub fn as_pool(&self) -> Result<DicePool, DiceParseError> {
        debug!("ParsedDiceExpression: {:#?}", self);
        let kd = KNOWN_DICE.lock().unwrap();
        let new_die: fn() -> DynDie = match &self.die_type {
            None => return Err(DiceParseError::NoTypeFound),
            Some(die_type) => {
                debug!("die_type: {:#?}", die_type);
                if !kd.contains_key(die_type) {
                    debug!("kd: {:#?}", kd);
                    return Err(DiceParseError::DiceTypeNotFound(die_type.into()));
                }
                *kd.get(die_type).unwrap()
            }
        };
        let die_count = self.die_count.unwrap_or(1);
        let mut pool = DicePool::new();
        for _ in 0..die_count {
            pool.add_die(new_die())
        }
        Ok(pool)
    }

    pub fn roll(&self) -> Result<RollResults, DiceParseError> {
        let mut pool: DicePool = match self.as_pool() {
            Ok(p) => p,
            Err(e) => return Err(e),
        };
        let mut results = pool.roll_all();
        debug!("Initial results: {results:#?}");

        // No modifier, no extra work
        if self.mod_type.is_none() {
            return Ok(results);
        }

        results.sort_by(|a, b| a.partial_cmp(b).unwrap());
        debug!("Results after sorting: {results:#?}");

        let mod_count = self.mod_count.unwrap_or(1);
        let mod_type = self.mod_type.as_ref().unwrap().as_str();
        debug!("mod_type: {mod_type:#?}, mod_count: {mod_count:#?}");

        let new_results: Vec<DynDieFace> = match mod_type {
            "k" | "kh" => {
                debug!("Keep highest");
                results
                    .into_iter()
                    .rev()
                    .take(mod_count)
                    .collect()
            }
            "kl" => {
                debug!("Keep lowest");
                results
                    .into_iter()
                    .take(mod_count)
                    .collect()
            }
            "d" | "dh" => {
                debug!("Drop highest");
                results
                    .into_iter()
                    .skip(mod_count)
                    .collect()
            }
            "dl" => {
                debug!("Drop lowest");
                results
                    .into_iter()
                    .rev()
                    .skip(mod_count)
                    .collect()
            }
            "kc" | "dc" => {
                debug!("Player's choice");
                // Returning here instead of converting to Vec only to have it convert back
                return Ok(results);
            }
            _ => {
                unreachable!("mod_type: {mod_type:#?}");
            }
        };
        Ok(new_results.into())
    }
}

pub fn parse_roll(dice_exp: &str) -> Result<ParsedDiceExpression, DiceParseError> {
    debug!("dice_exp: {dice_exp:#?}");
    debug!("Rule::multi_die_mod: {:#?}", Rule::multi_die_mod);
    match DiceParser::parse(Rule::multi_die_mod, dice_exp) {
        Ok(pairs) => {
            debug!("pairs: {pairs:#?}");
            let mut pde = ParsedDiceExpression::default();
            for pair in pairs {
                debug!("pair: {pair:#?}");
                parse_multi_die_mod(&mut pde, pair);
            }
            Ok(pde)
        }
        Err(_e) => Err(DiceParseError::UnrecognizedFormat(dice_exp.into())),
    }
}

fn parse_multi_die_mod(mut pde: &mut ParsedDiceExpression, pair: Pair<Rule>) {
    for inner in pair.into_inner() {
        debug!("inner: {inner:#?}");
        match inner.as_rule() {
            Rule::count => parse_die_count(&mut pde, &inner),
            Rule::die_type => parse_die_type(&mut pde, &inner),
            Rule::modifier => parse_modifier(&mut pde, inner),

            _ => {
                unreachable!("No parsing rule should match here.  (inner: {inner:?})")
            }
        }
    }
}

fn parse_die_type(pde: &mut &mut ParsedDiceExpression, pair: &Pair<Rule>) {
    pde.die_type = Some(pair.as_str().into())
}

fn parse_die_count(pde: &mut &mut ParsedDiceExpression, pair: &Pair<Rule>) {
    pde.die_count = pair.as_str().parse().ok()
}

fn parse_modifier(mut pde: &mut ParsedDiceExpression, pair: Pair<Rule>) {
    for die_mod in pair.into_inner() {
        debug!("die_mod: {:#?}", die_mod);
        match die_mod.as_rule() {
            Rule::count => parse_modifier_count(&mut pde, die_mod),
            Rule::modifier_prefix => parse_modifier_prefix(&mut pde, die_mod),

            _ => unreachable!(
                "No parsing rule should match here.  (die_mod: {:?})",
                die_mod
            ),
        }
    }
}

fn parse_modifier_prefix(pde: &mut ParsedDiceExpression, pair: Pair<Rule>) {
    pde.mod_type = Some(pair.as_str().into());
}

fn parse_modifier_count(pde: &mut ParsedDiceExpression, pair: Pair<Rule>) {
    pde.mod_count = pair.as_str().parse().ok();
}

#[cfg(test)]
mod tests {
    use std::sync::Once;
    use super::*;
    use log::{LevelFilter, set_max_level};
    use rstest::{fixture, rstest};
    use std::sync::Once;

    static ONE_TIME_SETUP: Once = Once::new();

    #[fixture]
    fn setup() {
        ONE_TIME_SETUP.call_once(|| {
            colog::default_builder()
                .is_test(true)
                .filter(None, LevelFilter::Debug)
                .init();
        });

        register_default_dice();
    }

    #[rstest]
    fn test_register_die_constructor(
        #[values("d4", "d6", "d8", "d10", "d12", "d20", "dplanet", "dzodiac", "coin")]
        die_type_name: &str,
        _setup: (),
    ) {
        let kd = KNOWN_DICE.lock().unwrap();
        assert!(kd.contains_key(die_type_name));
    }

    #[rstest]
    fn test_parse_roll_valid(_setup: ()) {
        let expr = "6d8";
        let parsed = parse_roll(expr).unwrap();
        assert_eq!(parsed.die_count, Some(6));
        assert_eq!(parsed.die_type, Some("d8".to_string()));
    }

    #[rstest]
    fn test_parse_roll_invalid_format(_setup: ()) {
        let expr = "6@8";
        let parsed = parse_roll(expr);
        assert!(parsed.is_err(), "Expecting an error, got {:?}", parsed);
        assert_eq!(
            parsed.err().unwrap(),
            DiceParseError::UnrecognizedFormat(expr.to_string())
        );
    }

    #[rstest]
    fn test_parse_roll_unknown_die_type(_setup: ()) {
        let expr = "6x8";
        let parsed = parse_roll(expr).unwrap();
        let result = parsed.roll();
        assert!(result.is_err(), "Expecting an error, got {:?}", result);
        assert_eq!(
            result.err().unwrap(),
            DiceParseError::DiceTypeNotFound("x8".into())
        );
    }

    #[rstest]
    fn test_roll_dice(_setup: ()) {
        let expr = "6d8";
        let parsed = parse_roll(expr).unwrap();
        let results = parsed.roll().unwrap();
        assert_eq!(results.len(), 6);
    }

    #[rstest]
    fn test_roll_with_modifier_keep_highest(_setup: ()) {
        let expr = "6d8:kh2";
        let parsed = parse_roll(expr).unwrap();
        let results = parsed.roll().unwrap();
        assert_eq!(results.len(), 2);
    }

    #[rstest]
    fn test_roll_with_modifier_keep_lowest(_setup: ()) {
        let expr = "6d8:kl2";
        let parsed = parse_roll(expr).unwrap();
        let results = parsed.roll().unwrap();
        assert_eq!(results.len(), 2);
    }

    #[rstest]
    fn test_roll_with_modifier_drop_highest(_setup: ()) {
        let expr = "6d8:dh2";
        let parsed = parse_roll(expr).unwrap();
        let results = parsed.roll().unwrap();
        assert_eq!(results.len(), 4);
        // Implement the logic for dropping highest and then test
    }

    #[rstest]
    fn test_roll_with_modifier_drop_lowest(_setup: ()) {
        let expr = "6d8:dl2";
        let parsed = parse_roll(expr).unwrap();
        let results = parsed.roll().unwrap();
        assert_eq!(results.len(), 4);
        // Implement the logic for dropping lowest and then test
    }
}
