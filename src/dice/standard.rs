use crate::dice::{
    Die, DieFace, DieFaceError, DieRng, DynDieFace, NewDie, RngParam, SortableDieFace,
};
use lazy_static::lazy_static;
use std::any::Any;
use std::collections::HashMap;
use std::fmt::{Debug, Display, Formatter};

use crate::die_face;
use std::hash::Hash;
use std::ops::Add;
use std::sync::{Arc, Mutex};
use uuid::Uuid;

lazy_static! {
    static ref META_REGISTRY: Mutex<HashMap<usize, Arc<MetaStandardDie>>> =
        Mutex::new(HashMap::new());
}

/// Shared information for multiple instances of the same sized StandardDie.
pub struct MetaStandardDie {
    name: Arc<str>,
    description: Arc<str>,
    faces: Vec<NumFace>,
}

impl MetaStandardDie {
    fn new<const SIDES: usize>(sides: usize) -> Self {
        let faces = Vec::from(standard_faces::<SIDES>());
        Self {
            name: format!("Standard D{}", sides).into(),
            description: format!("A standard die with {} sides.", sides).into(),
            faces,
        }
    }
}

/// Wrapper for numbered faces for dice.
#[derive(Copy, Clone, Debug, Eq, PartialEq, Hash, Ord, PartialOrd, Default, TypeInfo)]
pub struct NumFace(i8);

impl NumFace {
    pub fn num(&self) -> i8 {
        self.0
    }
}

die_face!(NumFace: display debug cmp display);

impl Display for NumFace {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl From<NumFace> for DynDieFace {
    fn from(value: NumFace) -> Self {
        DynDieFace {
            die_face: Box::new(value),
        }
    }
}

impl TryFrom<DynDieFace> for NumFace {
    type Error = DieFaceError;

    fn try_from(value: DynDieFace) -> Result<Self, Self::Error> {
        value
            .die_face
            .as_any()
            .downcast_ref::<NumFace>()
            .cloned()
            .ok_or(DieFaceError::CastingError)
    }
}

impl From<NumFace> for isize {
    fn from(value: NumFace) -> Self {
        value.0 as Self
    }
}

impl AsRef<i8> for NumFace {
    fn as_ref(&self) -> &i8 {
        &self.0
    }
}

impl SortableDieFace for NumFace {}

impl Add for NumFace {
    type Output = isize;

    fn add(self, rhs: Self) -> Self::Output {
        (self.0 + rhs.0).into()
    }
}

impl Add<isize> for NumFace {
    type Output = isize;

    fn add(self, rhs: isize) -> Self::Output {
        (self.0 as isize) + rhs
    }
}

impl Add<NumFace> for isize {
    type Output = isize;

    fn add(self, rhs: NumFace) -> Self::Output {
        self + (rhs.0 as isize)
    }
}

impl TryFrom<Box<dyn DieFace>> for Box<NumFace> {
    type Error = Box<dyn Any>;

    fn try_from(face: Box<dyn DieFace>) -> Result<Self, Self::Error> {
        let any_face: Box<dyn Any> = face.into();
        any_face.downcast::<NumFace>()
    }
}

/// Helper function to get an array of standard faces for `SIDES`-sided die.
const fn standard_faces<const SIDES: usize>() -> [NumFace; SIDES] {
    let mut faces: [NumFace; SIDES] = [NumFace(0); SIDES];
    let mut i = 1;
    while i <= SIDES {
        faces[i - 1] = NumFace(i as i8);
        i += 1;
    }
    faces
}

/// Standard die of `SIDES` sides with numbered faces 1 to `SIDES`
pub struct StandardDie<const SIDES: usize> {
    id: Uuid,
    meta: Arc<MetaStandardDie>,
    current_face: NumFace,
    rng: DieRng,
    // dyn_die: Option<DynDie>,
}

impl<const SIDES: usize> StandardDie<SIDES> {
    pub(crate) const FACES: [NumFace; SIDES] = standard_faces::<SIDES>();
}

impl<const SIDES: usize> PartialEq for StandardDie<SIDES> {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl<const SIDES: usize> Eq for StandardDie<SIDES> {}

impl<const SIDES: usize> Hash for StandardDie<SIDES> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}

impl<const SIDES: usize> NewDie for StandardDie<SIDES> {
    fn new_rng(rng: RngParam) -> Self
    where
        Self: Sized,
    {
        let mut meta_registry = META_REGISTRY.lock().unwrap();
        let meta = meta_registry
            .entry(SIDES)
            .or_insert_with(|| {
                let meta = MetaStandardDie::new::<SIDES>(SIDES);
                Arc::new(meta)
            })
            .clone();

        let mut rng = DieRng::new(rng, SIDES);
        let current_face = { meta.faces[rng.next()] };

        Self {
            id: Uuid::new_v4(),
            meta,
            current_face,
            rng,
            //dyn_die: None,
        }
    }
}

impl<const SIDES: usize> Die for StandardDie<SIDES> {
    fn rng(&self) -> DieRng {
        self.rng.clone()
    }

    fn get_current_face(&self) -> DynDieFace {
        DynDieFace {
            die_face: Box::new(self.current_face),
        }
    }

    fn set_current_face(&mut self, face: DynDieFace) {
        self.current_face = *face
            .value()
            .expect("`face` hold a value of type NumFace");
    }

    fn num_sides(&self) -> usize {
        SIDES
    }

    fn faces(&self) -> Box<dyn Iterator<Item = DynDieFace> + '_> {
        Self::model_faces().unwrap()
    }

    fn model_faces() -> Option<Box<dyn Iterator<Item = DynDieFace> + 'static>> {
        Some(Box::new(
            Self::FACES
                .into_iter()
                .map(|face| DynDieFace::new(dyn_clone::clone_box(&face))),
        ))
    }

    fn name(&self) -> Arc<str> {
        self.meta.name.clone()
    }

    fn description(&self) -> Arc<str> {
        self.meta.description.clone()
    }
}

/* Predefined dice */
pub type D4 = StandardDie<4>;
pub type D6 = StandardDie<6>;
pub type D8 = StandardDie<8>;
pub type D10 = StandardDie<10>;
pub type D12 = StandardDie<12>;
pub type D20 = StandardDie<20>;

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;
    use rstest_reuse::*;

    #[template]
    #[rstest]
    #[case::d4(D4::new, 4)]
    #[case::d6(D6::new, 6)]
    #[case::d8(D8::new, 8)]
    #[case::d10(D10::new, 10)]
    #[case::d12(D12::new, 12)]
    #[case::d20(D20::new, 20)]
    fn die_size_cases<const N: usize>(#[case] new_die: fn() -> StandardDie<N>, #[case] size: i8) {}

    #[apply(die_size_cases)]
    fn test_num_sides<const N: usize>(
        #[case] new_die: fn() -> StandardDie<N>,
        #[case] expected: usize,
    ) {
        let die = new_die();
        assert_eq!(expected, die.num_sides())
    }

    #[apply(die_size_cases)]
    fn test_roll<const N: usize>(#[case] new_die: fn() -> StandardDie<N>, #[case] max: i8) {
        let mut die = new_die();
        let range = 1..=max;
        for _i in 0..20 {
            die.roll();
            let result = die
                .get_current_face()
                .value::<NumFace>()
                .expect("Should be a NumFace");
            assert!(range.contains(&result.num()))
        }
    }

    // Helper function to roll a die multiple times and count the occurrences of each outcome
    fn roll_and_count<const N: usize>(
        die: &mut StandardDie<N>,
        num_rolls: usize,
    ) -> HashMap<NumFace, usize> {
        let mut counts: HashMap<NumFace, usize> = HashMap::new();
        for _ in 0..num_rolls {
            die.roll();
            let outcome: Box<NumFace> = die
                .get_current_face()
                .value()
                .expect("Should be a NumFace");
            *counts.entry(*outcome).or_insert(0) += 1;
        }
        counts
    }

    // Test to check if the distribution of outcomes is uniform
    #[apply(die_size_cases)]
    fn test_distribution_bias<const N: usize>(
        #[case] new_die: fn() -> StandardDie<N>,
        #[case] _size: usize,
    ) {
        let mut die = new_die();
        let num_rolls = 100000; // Number of rolls to simulate
        let counts = roll_and_count(&mut die, num_rolls);

        // Calculate the expected frequency for each outcome
        let expected_frequency = num_rolls as f64 / N as f64;

        // Check if the actual frequency is within a reasonable range of the expected frequency
        for (outcome, count) in counts {
            let actual_frequency = count as f64;
            let delta = (actual_frequency - expected_frequency).abs() / num_rolls as f64;
            assert!(
                delta <= 0.01,
                "Outcome {:?} has a bias. Expected frequency: {:?}, Actual frequency: {:?}, Delta: {:?}",
                outcome,
                expected_frequency,
                actual_frequency,
                delta
            );
        }
    }

    #[test]
    fn test_meta_standard_die_initialization() {
        let meta = MetaStandardDie::new::<6>(6);
        assert_eq!(meta.faces.len(), 6);
        assert_eq!(meta.name.as_ref(), "Standard D6");
        assert_eq!(meta.description.as_ref(), "A standard die with 6 sides.");
    }

    #[test]
    fn test_num_face_equality() {
        let face1 = NumFace(1);
        let face2 = NumFace(1);
        let face3 = NumFace(2);
        assert_eq!(face1, face2);
        assert_ne!(face1, face3);
    }

    #[test]
    fn test_num_face_hashing() {
        use std::collections::hash_map::DefaultHasher;
        use std::hash::{Hash, Hasher};

        let face1 = NumFace(1);
        let face2 = NumFace(1);
        let face3 = NumFace(2);

        let mut hasher1 = DefaultHasher::new();
        face1.hash(&mut hasher1);
        let hash1 = hasher1.finish();

        let mut hasher2 = DefaultHasher::new();
        face2.hash(&mut hasher2);
        let hash2 = hasher2.finish();

        let mut hasher3 = DefaultHasher::new();
        face3.hash(&mut hasher3);
        let hash3 = hasher3.finish();

        assert_eq!(hash1, hash2);
        assert_ne!(hash1, hash3);
    }

    #[test]
    fn test_die_face_data_conversion() {
        let face = NumFace(1);
        let die_face_data: DynDieFace = face.into();
        let result: Result<Box<NumFace>, _> = die_face_data.value();
        assert!(result.is_ok());
        assert_eq!(*result.unwrap(), face);
    }

    #[apply(die_size_cases)]
    fn test_standard_die_initialization<const N: usize>(
        #[case] new_die: fn() -> StandardDie<N>,
        #[case] _size: usize,
    ) {
        let die = new_die();
        assert_eq!(die.num_sides(), N);
        assert_eq!(die.name().as_ref(), format!("Standard D{}", N));
        assert_eq!(
            die.description().as_ref(),
            format!("A standard die with {} sides.", N)
        );
    }

    #[apply(die_size_cases)]
    fn test_standard_die_name<const N: usize>(
        #[case] new_die: fn() -> StandardDie<N>,
        #[case] _size: usize,
    ) {
        let die = new_die();
        assert_eq!(die.name().as_ref(), format!("Standard D{}", N));
    }

    #[apply(die_size_cases)]
    fn test_standard_die_description<const N: usize>(
        #[case] new_die: fn() -> StandardDie<N>,
        #[case] _size: usize,
    ) {
        let die = new_die();
        assert_eq!(
            die.description().as_ref(),
            format!("A standard die with {} sides.", N)
        );
    }
}
