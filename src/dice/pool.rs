#![allow(dead_code)]

use std::cmp::Ordering;
use std::fmt::{Display, Formatter};
use std::slice::Iter;
use crate::dice::{Die, DynDieFace, DynDie};

/**
* Heterogeneous collection of dices that can be rolled all at once.
*/
pub struct DicePool {
    dice: Vec<DynDie>,
}

impl DicePool {
    pub fn new() -> Self {
        DicePool { dice: Vec::new() }
    }

    pub fn add_die(&mut self, die: DynDie) {
        self.dice.push(die);
    }

    pub fn die_at(&self, index: usize) -> &DynDie {
        &self.dice[index]
    }

    pub fn remove_die_at(&mut self, index: usize) -> DynDie {
        self.dice.remove(index)
    }

    /// Roll all dice in the pool
    pub fn roll_all(&mut self) -> RollResults {
        let mut results = Vec::with_capacity(self.dice.len());
        for die in &mut self.dice {
            die.roll();
            let face = die.get_current_face();
            results.push(face);
        }
        RollResults(results)
    }
}

#[derive(Debug, Default)]
pub struct RollResults(Vec<DynDieFace>);

impl RollResults {
    pub fn len(&self) -> usize {
        self.0.len()
    }

    pub fn iter(&self) -> Iter::<DynDieFace> {
        self.0.iter()
    }

    pub fn sort_by<F>(&mut self, compare: F)
    where
        F: FnMut(&DynDieFace, &DynDieFace) -> Ordering,
    {
        self.0.sort_by(compare);
    }
}

impl From<Vec<DynDieFace>> for RollResults {
    fn from(value: Vec<DynDieFace>) -> Self {
        RollResults(value)
    }
}

impl From<RollResults> for Vec<DynDieFace> {
    fn from(value: RollResults) -> Self {
        value.0
    }
}

impl IntoIterator for RollResults {
    type Item = DynDieFace;
    type IntoIter = <Vec<DynDieFace> as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}


impl Display for RollResults {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        let v = &self.0;
        if v.is_empty() {
            return write!(f, "<No results>");
        }
        for (i, item) in v.iter().enumerate() {
            if i != 0 {
                write!(f, ", ")?;
            }
            write!(f, "{}", item)?;
        }
        write!(f, "")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::any::{TypeId};

    use crate::dice::{
        astro::{DPlanet, DZodiac, Planet, Zodiac},
        coin::{Coin, CoinFace},
        standard::{NumFace, D10, D12, D20, D4, D6, D8},
        DynDie,
    };

    use rstest::{fixture, rstest};

    #[fixture]
    fn pool() -> DicePool {
        DicePool::new()
    }

    #[rstest]
    fn test_empty_pool(mut pool: DicePool) {
        let results: RollResults = pool.roll_all();

        assert_eq!(results.len(), 0)
    }

    #[rstest]
    #[case::d4(DynDie::new::<D4>, TypeId::of::<NumFace>())]
    #[case::d6(DynDie::new::<D6>, TypeId::of::<NumFace>())]
    #[case::d8(DynDie::new::<D8>, TypeId::of::<NumFace>())]
    #[case::d10(DynDie::new::<D10>, TypeId::of::<NumFace>())]
    #[case::d12(DynDie::new::<D12>, TypeId::of::<NumFace>())]
    #[case::d20(DynDie::new::<D20>, TypeId::of::<NumFace>())]
    #[case::coin(DynDie::new::<Coin>, TypeId::of::<CoinFace>())]
    #[case::zodiac(DynDie::new::<DZodiac>, TypeId::of::<Zodiac>())]
    #[case::planet(DynDie::new::<DPlanet>, TypeId::of::<Planet>())]
    fn test_pool_of_one(
        mut pool: DicePool,
        #[case] new_die: fn() -> DynDie,
        #[case] face_type_id: TypeId,
    ) {
        let dyn_die = new_die();
        pool.add_die(dyn_die);

        let results: RollResults = pool.roll_all();

        assert_eq!(results.len(), 1);
        let inner_type_id = (*results.0[0].dyn_value()).type_id();
        assert_eq!(inner_type_id, face_type_id);
    }

    #[rstest]
    #[case::d4(DynDie::new::<D4>, TypeId::of::<NumFace>(), D4::model_faces())]
    #[case::d6(DynDie::new::<D6>, TypeId::of::<NumFace>(), D6::model_faces())]
    #[case::d8(DynDie::new::<D8>, TypeId::of::<NumFace>(), D8::model_faces())]
    #[case::d10(DynDie::new::<D10>, TypeId::of::<NumFace>(), D10::model_faces())]
    #[case::d12(DynDie::new::<D12>, TypeId::of::<NumFace>(), D12::model_faces())]
    #[case::d20(DynDie::new::<D20>, TypeId::of::<NumFace>(), D20::model_faces())]
    #[case::coin(DynDie::new::<Coin>, TypeId::of::<CoinFace>(), Coin::model_faces())]
    #[case::zodiac(DynDie::new::<DZodiac>, TypeId::of::<Zodiac>(), DZodiac::model_faces())]
    #[case::planet(DynDie::new::<DPlanet>, TypeId::of::<Planet>(), DPlanet::model_faces())]
    fn test_pool_of_10_same(
        mut pool: DicePool,
        #[case] new_die: fn() -> DynDie,
        #[case] face_type_id: TypeId,
        #[case] faces: Option<Box<dyn Iterator<Item=DynDieFace> + 'static>>,
    ) {
        for _ in 0..10 {
            pool.add_die(new_die())
        }

        let results: RollResults = pool.roll_all();

        assert_eq!(results.len(), 10);
        assert!(results.0
            .iter()
            .all(|res| (*res.dyn_value()).type_id() == face_type_id));

        match faces {
            None => {}
            Some(faces_iter) => {
                let die_faces: Vec<DynDieFace> = faces_iter.collect();
                assert!(results
                    .iter()
                    .all(|res| die_faces.contains(res)))
            }
        }
    }

    #[rstest]
    fn test_pool_of_all_different(mut pool: DicePool) {
        pool.add_die(DynDie::new::<D4>());
        pool.add_die(DynDie::new::<D6>());
        pool.add_die(DynDie::new::<D8>());
        pool.add_die(DynDie::new::<D10>());
        pool.add_die(DynDie::new::<D12>());
        pool.add_die(DynDie::new::<D20>());
        pool.add_die(DynDie::new::<DZodiac>());
        pool.add_die(DynDie::new::<DPlanet>());
        pool.add_die(DynDie::new::<Coin>());

        let results: RollResults = pool.roll_all();

        assert_eq!(pool.dice.len(), results.len());
        // TODO: Assert all correct type of die face
        // TODO: Assert all in faces of corresponding die
    }
}
