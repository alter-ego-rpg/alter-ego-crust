// #![feature(generic_const_exprs)]
// #![feature(type_alias_impl_trait)]
#![feature(trait_upcasting)]
#![feature(const_type_id)]
// #![feature(dispatch_from_dyn)]
// #![feature(unboxed_closures)]
#![allow(incomplete_features)]
// #![allow(unused_imports)]

extern crate core;
extern crate type_info;
#[macro_use]
extern crate type_info_derive;

pub mod character;
pub mod dice;
pub mod util;

#[macro_use]
pub mod macros;

#[cfg(test)]
#[allow(unused_imports)]
use rstest_reuse;
